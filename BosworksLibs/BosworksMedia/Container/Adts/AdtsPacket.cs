﻿using System;
using Bosworks.Buffers;
using Bosworks.MemoryPool;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public enum AdtsCaptureMode { FullPacket = 0, HeaderOnly = 1 }
    public enum AdtsParseState  { SyncError = -1, HeaderWait = 0, PayloadWait = 1, PacketReady = 2, SyncFound = 3, SyncLost = 4 }
    
    public class AdtsPacket
    {
        public AdtsHeader Header;

        private BufferAllocator<ArraySegment<byte>> _allocator;
        private ArraySegment<byte> _memoryRef;
        private readonly IAdtsHeaderFactory _headerFactory;

        private int _writeIdx;       
        private bool _headerLoaded;

        public AdtsCaptureMode CaptureMode { get; private set; }
        public AdtsParseState ParseState { get; private set; }

        public ArraySegment<byte> GetHeaderSegment()  { return new ArraySegment<byte>(_memoryRef.Array, _memoryRef.Offset, 7); }
        public ArraySegment<byte> GetPayloadSegment() { return new ArraySegment<byte>(_memoryRef.Array, 7, Header.FrameLen - 7); }
        public ArraySegment<byte> GetFullSegment()    { return _memoryRef; }

        private int _bytesNeeded;
        public int BytesNeeded 
        {
          get { return _bytesNeeded; }
          private set
          {
              if (value < 0)
              {
                  ParseState = AdtsParseState.SyncError;
                  _bytesNeeded = 0;
                  throw new ArgumentOutOfRangeException("value", "AdtsPacket BytesNeeded below zero");
              }
              _bytesNeeded = value;
          }
        }

        /* Must be frame synced already...      */
        public int Write(ArraySegment<byte> segIn)
        {
            if (_writeIdx == 0 && segIn.Array[segIn.Offset] != 0xFF)
            {
                ParseState = AdtsParseState.SyncError;
                return 0;
            }

            var bounds = new ContainerRange(segIn.Count, BytesNeeded);           
            BytesNeeded -= bounds.AmountToCopy;
        
            if (!_headerLoaded || (_headerLoaded && CaptureMode == AdtsCaptureMode.FullPacket))
            {
                _writeIdx += _allocator.WriteBuffer(segIn, _memoryRef, _writeIdx, bounds.AmountToCopy);
            }
                        
            if (_writeIdx >= 7 && !_headerLoaded)
            {
                Header = _headerFactory.CreateInstance(_memoryRef);                
                _headerLoaded = true;

                BytesNeeded = Header.FrameLen - Header.Length;
                if (Header.HasCrc) BytesNeeded += 2;

                ParseState = AdtsParseState.PayloadWait;

                if (CaptureMode == AdtsCaptureMode.FullPacket)
                    _memoryRef = _allocator.ReallocBuffer(_memoryRef, Header.FrameLen);
            }

            if (BytesNeeded == 0) ParseState = AdtsParseState.PacketReady;

            return bounds.AmountToCopy;
        }

        public void SetAllocator(BufferAllocator<ArraySegment<byte>> alloc)
        {
            _allocator = alloc;
            ArraySegPool.Attach(_allocator);
        }

        protected void resetState()
        {
            ParseState = AdtsParseState.HeaderWait;

            _headerLoaded = false;
            _writeIdx = 0;
            BytesNeeded = 7;
            _memoryRef = _allocator.AllocBuffer(9);
        }

        public AdtsPacket()
        {
            if (_headerFactory == null) _headerFactory = new AdtsHeader();
            
            if (_allocator == null) SetAllocator(new BufferAllocator<ArraySegment<byte>>());            
            resetState();            
        }

        public AdtsPacket(AdtsCaptureMode capMode, IAdtsHeaderFactory hdrFactory = null, BufferAllocator<ArraySegment<byte>> allocator = null)
            : this()
        {
            if (hdrFactory != null) _headerFactory = hdrFactory.CreateInstance();
            if (allocator != null) SetAllocator(allocator);            
            CaptureMode = capMode;
        }

        public AdtsPacket(ArraySegment<byte> segIn, AdtsCaptureMode capMode, IAdtsHeaderFactory hdrFactory = null, BufferAllocator<ArraySegment<byte>> allocator = null)
            : this(capMode, hdrFactory, allocator)
        {            
            Write(segIn);
        }
       
    }

   

}
