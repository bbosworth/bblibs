﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public class AdtsMp3Header : AdtsHeader
    {
        public AdtsChannelMode ChannelMode { get; set; }

        public override bool LoadFrom(ArraySegment<byte> srcBytes)
        {            
            var data = srcBytes.Take(7);
            if (!base.LoadFrom(srcBytes)) return false;

            MpegVersion = (AdtsMpegVersion) ((data.Array[data.Offset + 1] & 0x18) >> 3);
            Layer =       (AdtsLayer)       ((data.Array[data.Offset + 1] & 0x06) >> 1);
            ChannelMode = (AdtsChannelMode) ((data.Array[data.Offset + 3] & 0xC0) >> 6);
            HasCrc =                        ((data.Array[data.Offset + 1] & 0x01) == 0);            

            Length = (HasCrc ? 9 : 7);

            var b = (data.Array[data.Offset + 2] & 0xF0) >> 4;
            Bitrate = AdtsHelper.GetBitrate(MpegVersion, Layer, b);

            if (Layer != AdtsLayer.AacProfile)
            {
                b = (data.Array[data.Offset + 2] & 0x0C) >> 2;
                SampleRate = AdtsHelper.GetSampleRate(MpegVersion, b);
            }

            if (Layer == AdtsLayer.Layer1) SamplesPerFrame = 384;
            if (Layer == AdtsLayer.Layer2 || Layer == AdtsLayer.Layer3) SamplesPerFrame = 1152;

            b = (data.Array[data.Offset + 2] & 0x02) >> 1;
            if (b == 1)
            {
                if (Layer == AdtsLayer.Layer3 || Layer == AdtsLayer.Layer2) Padding = 1;
                if (Layer == AdtsLayer.Layer1) Padding = 4;

            }
            else Padding = 0;

            if (Layer == AdtsLayer.Layer2 || Layer == AdtsLayer.Layer3)
                FrameLen = 144*(Bitrate*1000)/SampleRate + Padding;

            if (Layer == AdtsLayer.Layer1)
                FrameLen = (12*((Bitrate*1000)/SampleRate) + Padding)*4;

            return true;
        }

        public override AdtsHeader CreateInstance(ArraySegment<byte> data)
        {
            var hdr = new AdtsMp3Header();
            hdr.LoadFrom(data);
            return hdr;
        }

        public override AdtsHeader CreateInstance()
        {
            return new AdtsMp3Header();
        }
    }

}
