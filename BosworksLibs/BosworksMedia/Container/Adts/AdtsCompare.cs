﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosworks.Container.Adts
{
    [Flags]
    public enum AdtsFields
    {
        Nothing = 0,
        MpegVersion = 0x01,
        Layer       = 0x02,
        ChannelMode = 0x04,
        Bitrate     = 0x08,
        SampleRate  = 0x10,
        Protection  = 0x20,

        VersionLayerChannel = MpegVersion + Layer + ChannelMode,
        All = VersionLayerChannel + Bitrate + SampleRate + Protection
    }

    public static class AdtsCompare
    {
        public static AdtsFields CompareTo(this AdtsMp3Header srcHeader, AdtsMp3Header theOther, AdtsFields checks)
        {
            var res = AdtsFields.Nothing;

            if ((checks & AdtsFields.MpegVersion) == AdtsFields.MpegVersion)
                res += (int)(srcHeader.MpegVersion != theOther.MpegVersion ? AdtsFields.MpegVersion : 0);

            if ((checks & AdtsFields.Layer) == AdtsFields.Layer)
                res += (int)(srcHeader.Layer != theOther.Layer ? AdtsFields.Layer : 0);

            if ((checks & AdtsFields.ChannelMode) == AdtsFields.ChannelMode)
                res += (int)(srcHeader.ChannelMode != theOther.ChannelMode ? AdtsFields.ChannelMode : 0);

            if ((checks & AdtsFields.Bitrate) == AdtsFields.Bitrate)
                res += (int)(srcHeader.Bitrate != theOther.Bitrate ? AdtsFields.Bitrate : 0);

            if ((checks & AdtsFields.SampleRate) == AdtsFields.SampleRate)
                res += (int)(srcHeader.SampleRate != theOther.SampleRate ? AdtsFields.SampleRate : 0);

            if ((checks & AdtsFields.Protection) == AdtsFields.Protection)
                res += (int)(srcHeader.HasCrc != theOther.HasCrc ? AdtsFields.Protection : 0);
            
            return res;
        }
    }
}
