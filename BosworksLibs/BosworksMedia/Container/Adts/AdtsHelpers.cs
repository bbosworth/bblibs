﻿using System;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public static class AdtsHelper
    {
        public static int GetBitrate(AdtsMpegVersion aid, AdtsLayer layer, int bitrateIdx)
        {
            if (aid == AdtsMpegVersion.MpegV1) return AdtsStaticData.MpegV1Bitrates[(int)layer, bitrateIdx];
            return -1;
        }

        public static int GetSampleRate(AdtsMpegVersion aid, int sampleIdx)
        {
            if (aid == AdtsMpegVersion.MpegV1) return AdtsStaticData.MpegV1SampleRates[sampleIdx];
            if (aid == AdtsMpegVersion.MpegV2) return AdtsStaticData.MpegV2SampleRates[sampleIdx];
            if (aid == AdtsMpegVersion.MpegV25) return AdtsStaticData.MpegV25SampleRates[sampleIdx];
            return -1;
        }
    }

}
