﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public class AdtsReaderBase
    {        
        public bool FoundMpegSync { get; private set; }
        public AdtsCaptureMode CaptureMode;

        protected AdtsPacket _activePacket = null;
        protected IAdtsHeaderFactory _headerFactory = null;
       
        protected virtual void notifyPacketReady(AdtsPacket newPacket) { }
        protected virtual void notifySkippedData(byte[] dataIn, int startIdx, int count) { }
        protected virtual void notifySyncStateChange(int pos, AdtsParseState state) { }

        protected virtual int attemptSync(byte[] dataIn, int ofs, int count)
        {
            var skipAhead = 0;                        
            var syncPos = AdtsSyncFrame.FindSync(dataIn, ofs, count);

            if (syncPos.DataIndex <= -1)
            {
                notifySkippedData(dataIn, ofs, count);
                return 0;
            }
            
            FoundMpegSync = true;
            notifySyncStateChange(syncPos.DataIndex, AdtsParseState.SyncFound);
            skipAhead = syncPos.DataIndex - ofs;

            notifySkippedData(dataIn, ofs, skipAhead);
            _activePacket = new AdtsPacket(CaptureMode);

            return skipAhead;            
        }

        public virtual int Write(byte[] dataIn, int ofs, int count)
        {            
            var readIdx = ofs;

            while (readIdx < count)
            {
                if (!FoundMpegSync)
                {
                    readIdx += attemptSync(dataIn, readIdx, count - readIdx);
                    if (!FoundMpegSync) return readIdx - ofs;
                }

                var taken = _activePacket.Write(new ArraySegment<byte>(dataIn, readIdx, count - readIdx));

                if (_activePacket.ParseState == AdtsParseState.SyncError)
                {
                    FoundMpegSync = false;
                    _activePacket = new AdtsPacket(CaptureMode);
                    notifySyncStateChange(readIdx, AdtsParseState.SyncLost);
                }
                else readIdx += taken;

                if (_activePacket.ParseState != AdtsParseState.PacketReady) continue;

                notifyPacketReady(_activePacket);
                _activePacket = new AdtsPacket(CaptureMode);
            }
            return readIdx - ofs;
        }

        public virtual void SetHeaderFactory(IAdtsHeaderFactory factory) { _headerFactory = factory; }

        public virtual void Reset()
        {
            FoundMpegSync = false;
            _activePacket = null;
        }

        public AdtsReaderBase()
        {
            FoundMpegSync = false;
            if (_headerFactory == null) _headerFactory = new AdtsHeader();
        }

        public AdtsReaderBase(AdtsCaptureMode capMode, IAdtsHeaderFactory hdrFactory = null)
        {
            CaptureMode = capMode;
            FoundMpegSync = false;            
            _headerFactory = hdrFactory;            
        }
    }
}
