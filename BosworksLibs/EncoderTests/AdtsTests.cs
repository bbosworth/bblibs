﻿using System;
using System.IO;
using Bosworks.Container;
using Bosworks.Container.Adts;
using Bosworks.Encoders;
using NUnit.Framework;
using Bosworks;
using Bosworks.Buffers;
using NUnit.Framework.Constraints;

namespace Bosworks
{
    /* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

    [TestFixture]
    public class AdtsTests
    {

        [Test]
        public void TestBoundaryCalc()
        {
            var pd = new ContainerRange(1026, 1024);

            Assert.That(pd.AmountLeftover, Is.EqualTo(2));
            Assert.That(pd.AmountToCopy, Is.EqualTo(1024));
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void ShortFailExceptionAdtsHeader()
        {
            var adtsHeader = new AdtsHeader();

            var shortbuf = new byte[4] {0xFF, 0xFB, 0x90, 0x00};
            adtsHeader.LoadFrom(new ArraySegment<byte>(shortbuf));
        }

        [Test]
        public void BadSyncAdtsHeader()
        {
            var adtsHeader = new AdtsHeader();

            var badSync = new byte[7] {0xFA, 0xFB, 0x90, 0x00, 0x00, 0x00, 0x02};
            adtsHeader.LoadFrom(new ArraySegment<byte>(badSync));
            Assert.That(adtsHeader.ValidSync, Is.EqualTo(false));
        }

        [Test]
        public void AdtsHeader()
        {
            var adtsHeader = new AdtsMp3Header();

            // Mpeg1 Layer3 128bit 44.1khz Stereo NoPad NoCrc
            var buf = new byte[7] {0xFF, 0xFB, 0x90, 0x00, 0x00, 0x00, 0x02};

            adtsHeader.LoadFrom(new ArraySegment<byte>(buf));

            Assert.That(adtsHeader.ValidSync, Is.EqualTo(true));

            Assert.That(adtsHeader.MpegVersion, Is.EqualTo(AdtsMpegVersion.MpegV1));
            Assert.That(adtsHeader.Bitrate, Is.EqualTo(128));
            Assert.That(adtsHeader.Layer, Is.EqualTo(AdtsLayer.Layer3));
            Assert.That(adtsHeader.SampleRate, Is.EqualTo(44100));
            Assert.That(adtsHeader.ChannelMode, Is.EqualTo(AdtsChannelMode.Stereo));
            Assert.That(adtsHeader.FrameLen, Is.EqualTo(417));
            Assert.That(adtsHeader.Padding, Is.EqualTo(0));
        }

        public void Mp3PacketAssert(AdtsPacket packet)
        {
            Assert.That(packet.Header.ValidSync, Is.EqualTo(true));

            Assert.That(packet.Header.MpegVersion, Is.EqualTo(AdtsMpegVersion.MpegV1));
            Assert.That(packet.Header.Bitrate, Is.EqualTo(128));
            Assert.That(packet.Header.Layer, Is.EqualTo(AdtsLayer.Layer3));
            Assert.That(packet.Header.SampleRate, Is.EqualTo(44100));

            Assert.That(packet.Header.FrameLen, packet.Header.Padding == 0 ? Is.EqualTo(417) : Is.EqualTo(418));
        }

        [Test]
        public void AdtsMp3FileHeaderOnlyParseTest()
        {
            var adts = new AdtsReader(AdtsCaptureMode.HeaderOnly);
            var fs = new FileStream(@"sym1.mp3", FileMode.Open);

            var buf = new byte[2048];
            var res = 0;

            adts.OnAdtsPacket += (sender, packet) => Mp3PacketAssert(packet);
            adts.OnSkipData += (sender, data, ofs, count) => Assert.That(count, Is.EqualTo(917).Or.EqualTo(128)); // ID3 junk in sym1.mp3
           
            while (fs.Position < fs.Length)
            {
                var readLen = fs.Read(buf, 0, 2048);
                res = adts.Write(buf, 0, readLen);
            }           

            fs.Dispose();
        }

        [Test]
        public void AdtsMp3FileFullPacketParseTest()
        {
            var adts = new AdtsReader(AdtsCaptureMode.FullPacket);
            var fs = new FileStream(@"sym1.mp3", FileMode.Open);

            var buf = new byte[2048];
            var res = 0;

            adts.OnAdtsPacket += (sender, packet) => Mp3PacketAssert(packet);
            adts.OnSkipData += (sender, data, ofs, count) => Assert.That(count, Is.EqualTo(917).Or.EqualTo(128)); // ID3 junk in sym1.mp3

            while (fs.Position < fs.Length)
            {
                var readLen = fs.Read(buf, 0, 2048);
                res = adts.Write(buf, 0, readLen);
            }            

            fs.Dispose();
        }

        [Test]
        public void AdtsAacFileFullPacketParseTest()
        {
            var adts = new AdtsReader(AdtsCaptureMode.FullPacket);
            var fs = new FileStream(@"test.aac", FileMode.Open);

            var buf = new byte[2048];
            var res = 0;

            var readLen = fs.Read(buf, 0, 2048);
            res = adts.Write(buf, 0, readLen);
            Assert.That(res, Is.GreaterThan(0));

            fs.Dispose();
        }

        [Test]
        public void AdtsHeaderCompareMapper()
        {
            var adtsHeader1 = new AdtsMp3Header();
            var adtsHeader2 = new AdtsMp3Header();

            // Mpeg1 Layer3 128bit 44.1khz Stereo NoPad NoCrc
            var buf = new byte[7] {0xFF, 0xFB, 0x90, 0x00, 0x00, 0x00, 0x02};
            adtsHeader1.LoadFrom(new ArraySegment<byte>(buf));
            adtsHeader2.LoadFrom(new ArraySegment<byte>(buf));

            var difMap = adtsHeader1.CompareTo(adtsHeader2, AdtsFields.All);
            Assert.That(difMap, Is.EqualTo(AdtsFields.Nothing));

            // change #2 to 64kbps bitrate, 32kbps samplerate
            buf = new byte[7] {0xFF, 0xFB, 0x58, 0x00, 0x00, 0x00, 0x02};
            adtsHeader2.LoadFrom(new ArraySegment<byte>(buf));

            difMap = adtsHeader1.CompareTo(adtsHeader2, AdtsFields.All);

            Assert.That(difMap & AdtsFields.MpegVersion, Is.EqualTo(AdtsFields.Nothing));
            Assert.That(difMap & AdtsFields.Bitrate, Is.EqualTo(AdtsFields.Bitrate));
            Assert.That(difMap & AdtsFields.SampleRate, Is.EqualTo(AdtsFields.SampleRate));

            difMap = adtsHeader1.CompareTo(adtsHeader2, AdtsFields.ChannelMode);
            Assert.That(difMap, Is.EqualTo(AdtsFields.Nothing));

        }

        [Test]
        public void AdtsHeaderFactory()
        {            
            // Mpeg1 Layer3 128bit 44.1khz Stereo NoPad NoCrc
            var buf = new byte[7] {0xFF, 0xFB, 0x90, 0x00, 0x00, 0x00, 0x02};
            var header = Bosworks.Container.Adts.AdtsHeaderFactory.AutoDetectHeader(new ArraySegment<byte>(buf));
            Assert.IsInstanceOfType(typeof(AdtsMp3Header), header);
            
        }
    }
}
