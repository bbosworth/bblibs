﻿using System;
using NUnit.Framework;
using Bosworks.Encoders.Lame;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace EncoderTests
{
    [TestFixture]
    public class LameEncoderTests
    {
        
        [Test]
        public void TestRawLibInitialization()
        {
            var config = new LameEncoderConfig();

            uint samples = 0;
            uint bufSize = 0;
            uint stream = 0;

            var res = LameMp3Wrap.beInitStream(config, ref samples, ref bufSize, ref stream);

            Assert.That(res, Is.EqualTo(LameResult.Ok));
            Assert.That(stream,  Is.GreaterThan(0));
            Assert.That(bufSize, Is.GreaterThan(0));
            Assert.That(samples, Is.GreaterThan(0));
        }

        [Test]
        public void TestManagedLibInitialization()
        {
            var config = new LameEncoderConfig();

            var lameLib = new LameMp3Encoder(config.SetMp3Output(44100, 128, ChannelMode.JointStereo));
            var res = lameLib.Initialize();

            Assert.That(res, Is.EqualTo(true));
            Assert.That(lameLib.StreamId, Is.GreaterThan(0));
            Assert.That(lameLib.Samples, Is.GreaterThan(0));
            Assert.That(lameLib.BufferSize, Is.GreaterThan(0));

            Assert.That(lameLib.Config.nMode, Is.EqualTo(ChannelMode.JointStereo));
            Assert.That(lameLib.Config.dwBitrate, Is.EqualTo(128));
            Assert.That(lameLib.Config.dwSampleRate, Is.EqualTo(44100));            
        }
    }
}
