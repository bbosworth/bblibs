﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bosworks.Buffers
{
    public struct ContainerRange
    {
        public int AmountToCopy;
        public int AmountLeftover;

        public ContainerRange(int srcLen, int destAvail)
        {
            if (srcLen < 0 || destAvail < 0)
                throw new ArgumentException(String.Format("Boundingdata: InvalidCastException srcLen {0}, {1}", srcLen, destAvail));

            AmountToCopy = srcLen > destAvail ? destAvail : srcLen;

            if (AmountToCopy < srcLen) AmountLeftover = srcLen - AmountToCopy;
            else AmountLeftover = 0;
        }

        public ContainerRange(int srcLen, ArraySegment<byte> dest) : this(srcLen, dest.Count) { }
        public ContainerRange(ArraySegment<byte> src, ArraySegment<byte> dest) : this(src.Count, dest.Count) { }  
    }

}
